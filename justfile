tidy:
    go mod tidy

build: tidy
     docker build -t indra:latest .

run: build
    docker run -p 8000:8000 indra:latest